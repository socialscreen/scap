interface Error {
    errorCode: string;
    errorText: string;
}

declare class InputSource {
    constructor();

    changeInputSource(success: () => void, failure: (error: Error) => void, options);
    getExternalInputList(success: () => void, failure: (error: Error) => void, options);
    getInputSourceStatus(success: () => void, failure: (error: Error) => void);
    initialize(success: () => void, failure: (error: Error) => void, options);
}
