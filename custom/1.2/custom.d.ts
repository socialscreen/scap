interface Error {
    errorCode: string;
    errorText: string;
}

interface ApplicationInfo { 
    allowAudioCapture: boolean;
    allowVideoCapture: boolean;
    appDescription: string;
    crossDomainSecurity: string;
    disalbeBackHistoryAPI: boolean;
    icon: string;
    id: string;
    inspectable: boolean;
    largeIcon: string;
    main: string;
    splashBackground: string;
    supportedOrientations: string[];
    title: string;
    type: string;
    useVideoTexture: string;
    useVirtualKeyboard: boolean;
    version: string;
    vendor: string;
}

interface GetKAMResponse {
    keepAliveMode: boolean;
}

interface SetKAMOptions {
    keepAliveMode: boolean;
}

interface Signage {
    addUSBAttachEventListener(success: () => void, failure: (error: Error) => void): void;
    removeUSBAttachEventListener(success: () => void, failure: (error: Error) => void): void;
    getwebOSVersion(success: (response: { webOSVersion: string }) => void, failure: (error: Error) => void): void;
    getApplicationInfo(success: (applicationInfo: ApplicationInfo) => void, failure: (error: Error) => void): void;
    switchApplication(success: () => void, failure: (error: Error) => void, params: { application: string }): void;
    disableApplication(success: () => void, failure: (error: Error) => void, params: { reset: boolean }): void;
}

interface Configuration {
    getPortControl(success: () => void, failure: (error: Error) => void, l): void;
    setPortControl(success: () => void, failure: (error: Error) => void, l): void;
    setEnterpriseCode(success: () => void, failure: (error: Error) => void, l): void;
    clearBrowsingData(success: () => void, failure: (error: Error) => void, l): void;
    setWhiteBalanceRGB(success: () => void, failure: (error: Error) => void, l): void;
    getWhiteBalanceRGB(success: () => void, failure: (error: Error) => void): void;
    setAvSync(success: () => void, failure: (error: Error) => void, l): void;
    getAvSync(success: () => void, failure: (error: Error) => void): void;
    setAvSyncSpeaker(success: () => void, failure: (error: Error) => void, l): void;
    getAvSyncSpeaker(success: () => void, failure: (error: Error) => void): void;
    setAvSyncBypass(success: () => void, failure: (error: Error) => void, l): void;
    getAvSyncBypass(success: () => void, failure: (error: Error) => void): void;
    setNoSignalImageStatus(success: () => void, failure: (error: Error) => void, l): void;
    getNoSignalImageStatus(success: () => void, failure: (error: Error) => void): void;
    getPowerOnOffHistory(success: () => void, failure: (error: Error) => void): void;
    setPowerOnStatus(success: () => void, failure: (error: Error) => void, l): void;
    getPowerOnStatus(success: () => void, failure: (error: Error) => void): void;
    setKAM(success: () => void, failure: (error: Error) => void, options: SetKAMOptions): void;
    getKAM(success: (res: GetKAMResponse) => void, failure: (error: Error) => void): void;
    changePassword(success: () => void, failure: (error: Error) => void, l): void;
    getNativePortraitMode(success: () => void, failure: (error: Error) => void): void;
    setNativePortraitMode(success: () => void, failure: (error: Error) => void, l): void;
    getWoWLAN(success: () => void, failure: (error: Error) => void): void;
    setWoWLAN(success: () => void, failure: (error: Error) => void, l): void;
}

interface VideoSync {
    setMaster(success: (response: { basetime: string }) => void, failure: (error: Error) => void, params: { ip: string, port: Number });
    setSlave(success: () => void, failure: (error: Error) => void, params: { ip: string, port: Number, basetime: string });
}

interface ERROR_CODE {
    COMMON: {
        OLD_WEBOS_VERSION: string;
        UNSUPPORTED_API: string;
        BAD_PARAMETERS: string;
        INTERNAL_ERROR: string;
        NOT_MONITORING: string;
        MEDIA_ERROR: string;
    };
    CONFIGURATION: {
        INVALID_PASSWORD_FORMAT: string;
        ACCESS_DENIED: string;
        INVALID_CONFIG: string;
    };
    APPLICATION: {
        SETTINGS_ERROR: string;
        NOT_INSTALLED: string;
    }
}

interface CLEARBROWSINGDATATYPES {
    ALL: string;
    APPCACHE: string;
    CACHE: string;
    CHANNELIDS: string;
    COOKIES: string;
    FILESYSTEMS: string;
    INDEXEDDB: string;
    LOCALSTORAGE: string;
    SERVICEWORKERS: string;
    WEBSQL: string;
}

interface AVSYNC {
    ON: string;
    OFF: string;
}

interface AVSYNCBYPASS {
    ON: string;
    OFF: string;
}

interface NOSIGNALIMAGE {
    ON: string;
    OFF: string;
}

interface POWERONSTATUS {
    POWERON: string;
    STANDBY: string;
    LASTSTATUS: string;
}

interface APPLICATION {
    ZIP_TYPE: string;
    IPK_TYPE: string;
    EXTERNAL_HDMI: string;
    EXTERNAL_HDMI1: string;
    EXTERNAL_HDMI2: string;
    EXTERNAL_HDMI3: string;
    EXTERNAL_HDMI4: string;
    EXTERNAL_RGB: string;
    EXTERNAL_DVI: string;
    EXTERNAL_DP: string;
    EXTERNAL_OPS: string;
}

interface NATIVEPORTRAIT {
    OFF: string;
    DEGREE_90: string;
    DEGREE_180: string;
    DEGREE_270: string;
}

declare class Custom {
    constructor();

    static readonly ERROR_CODE: ERROR_CODE;
    static readonly CLEARBROWSINGDATATYPES: CLEARBROWSINGDATATYPES;
    static readonly AVSYNC: AVSYNC;
    static readonly AVSYNCBYPASS: AVSYNCBYPASS;
    static readonly NOSIGNALIMAGE: NOSIGNALIMAGE;
    static readonly POWERONSTATUS: POWERONSTATUS;
    static readonly APPLICATION: APPLICATION;
    static readonly NATIVEPORTRAIT: NATIVEPORTRAIT;

    readonly VideoSync: VideoSync;
    readonly Configuration: Configuration;
    readonly Signage: Signage;
}
