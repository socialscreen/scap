interface Error {
    errorCode: string;
    errorText: string;
}

interface EddystoneFrame {
    UUID: string;
    URL: string;
}

interface GetSystemUsageInfoOptions {
    cpus?: boolean;
    memory?: boolean;
}

interface CPU {
    model?: string;
    times?: {
        user?: number;
        nice?: number;
        sys?: number;
        idle?: number;
        irq?: number;
    };
}

interface Memory {
    total?: number;
    free?: number;
    used?: number;
    buffer?: number;
    cached?: number;
}

interface GetSystemUsageInfoResponse {
    cpus?: CPU[];
    memory?: Memory;
}

interface NetworkInfo {
    signalLevel?: number;
    ssid?: string;
}

interface GetWifiListResponse {
    networkInfo: NetworkInfo[];
}

interface GetPlatformInfoResponse {
    modelName: string;
    serialNumber: string;
    firmwareVersion: string;
    hardwareVersion: string;
    sdkVersion: string;
    manufacturer: string;
}

interface GetNetworkInfoResponse {
    isInternetConnectionAvailable: boolean;
    wired: {
        state: string;
        interfaceName?: string;
        ipAddress?: string;
        netmask?: string;
        gateway?: string;
        onInternet?: string;
        method?: string;
        dns1?: string;
        dns2?: string;
        dns3?: string;
        dns4?: string;
        dns5?: string;
        ipv6?: {
            gateway?: string;
            ipAddress?: string;
            prefixLength?: string;
            method?: string;
        };
    };
    wifi: {
        state: string;
        interfaceName?: string;
        ipAddress?: string;
        netmask?: string;
        gateway?: string;
        onInternet?: string;
        method?: string;
        dns1?: string;
        dns2?: string;
    };
}

interface GetNetworkMacInfoResponse {
    wifiInfo?: {
        macAddress?: string;
    };
    wiredInfo?: {
        macAddress?: string;
    };
}

interface GetSensorValuesResponse {
    backlight: number;
    fan: {
        openLoop?: boolean[];
        closedLoop?: boolean[];
    };
    humidity: number;
    illuminance: number;
    temperature: number;
    checkscreen: {
        drawRGB: number;
        readRGB: number;
        colorValid: boolean;
    },
    rotation: string;
}

declare class DeviceInfo {
    constructor();

    static readonly EddystoneFrame: EddystoneFrame;

    getNetworkInfo(success: (response: GetNetworkInfoResponse) => void, failure: (error: Error) => void);
    setNetworkInfo(success: () => void, failure: (error: Error) => void, g);
    getBeaconInfo(success: () => void, failure: (error: Error) => void);
    setBeaconInfo(success: () => void, failure: (error: Error) => void, g);
    getSoftApInfo(success: () => void, failure: (error: Error) => void);
    setSoftApInfo(success: () => void, failure: (error: Error) => void, h);
    getSoftApClientInfo(success: () => void, failure: (error: Error) => void);
    getWifiList(success: (response: GetWifiListResponse) => void, failure: (error: Error) => void);
    connectWifi(success: () => void, failure: (error: Error) => void, g);
    startWps(success: () => void, failure: (error: Error) => void, g);
    stopWps(success: () => void, failure: (error: Error) => void);
    getNetworkMacInfo(success: (response: GetNetworkMacInfoResponse) => void, failure: (error: Error) => void);
    getPlatformInfo(success: (response: GetPlatformInfoResponse) => void, failure: (error: Error) => void);
    getSystemUsageInfo(success: (response: GetSystemUsageInfoResponse) => void, failure: (error: Error) => void, options: GetSystemUsageInfoOptions);
    setProxyInfo(success: () => void, failure: (error: Error) => void, g);
    getProxyInfo(success: () => void, failure: (error: Error) => void);
    setiBeaconInfo(success: () => void, failure: (error: Error) => void, g);
    getiBeaconInfo(success: () => void, failure: (error: Error) => void);
    setEddystoneInfo(success: () => void, failure: (error: Error) => void, g);
    getEddystoneInfo(success: () => void, failure: (error: Error) => void);
    getBlockedPortList(success: () => void, failure: (error: Error) => void);
    addBlockedPortList(success: () => void, failure: (error: Error) => void, g);
    deleteBlockedPortList(success: () => void, failure: (error: Error) => void, g);
    getSensorValues(success: (response: GetSensorValuesResponse) => void, failure: (error: Error) => void);
    setSensorValues(success: () => void, failure: (error: Error) => void, g);
    getHDBaseTMode(success: () => void, failure: (error: Error) => void);
    setHDBaseTMode(success: () => void, failure: (error: Error) => void, g);
    getNetworkCheckupInfo(success: () => void, failure: (error: Error) => void);
    setNetworkCheckupInfo(success: () => void, failure: (error: Error) => void, g);
}
