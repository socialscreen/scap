interface Error {
    errorCode: string;
    errorText: string;
}

interface SoundMode {
    Standard: string;
    Cinema: string;
    ClearVoice: string;
    Sports: string;
    Music: string;
    Game: string;
}

interface SpeakerType {
    SignageSpeaker: string;
    LGSoundSync: string;
}

declare class Sound {
    constructor();

    static readonly SoundMode: SoundMode;
    static readonly SpeakerType: SpeakerType;

    getSoundStatus(success: () => void, failure: (error: Error) => void);
    setVolumeLevel(success: () => void, failure: (error: Error) => void, options);
    setExternalSpeaker(success: () => void, failure: (error: Error) => void, options);
    setMuted(success: () => void, failure: (error: Error) => void, options);
    setSoundMode(success: () => void, failure: (error: Error) => void, options);
    getSoundMode(success: () => void, failure: (error: Error) => void);
    setSoundOut(success: () => void, failure: (error: Error) => void, options);
    getSoundOut(success: () => void, failure: (error: Error) => void);
}
