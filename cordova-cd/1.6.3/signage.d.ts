interface Error {
    errorCode: string;
    errorText: string;
}

type UNDEFINED = string;

interface OsdPortraitMode {
    ON: string;
    OFF: string;
}

interface ImgResolution {
    HD: string;
    FHD: string;
}

interface AutomaticStandbyMode {
    OFF: string;
    STANDBY_4HOURS: string;
}

interface IsmMethod {
    NORMAL: string;
    ORBITER: string;
    INVERSION: string;
    COLORWASH: string;
    WHITEWASH: string;
    WASHING_BAR: string;
    USER_IMAGE: string;
    USER_VIDEO: string;
}

interface FailoverMode {
    OFF: string;
    AUTO: string;
    MANUAL: string;
}

interface DigitalAudioInput {
    HDMI_DP: string;
    AUDIO_IN: string;
}

interface DpmMode {
    OFF: string;
    POWER_OFF_5SECOND: string;
    POWER_OFF_10SECOND: string;
    POWER_OFF_15SECOND: string;
    POWER_OFF_1MINUTE: string;
    POWER_OFF_3MINUTE: string;
    POWER_OFF_5MINUTE: string;
    POWER_OFF_10MINUTE: string;
}

interface KeyOperationMode {
    ALLOW_ALL: string;
    POWER_ONLY: string;
    BLOCK_ALL: string;
}

interface EventType {
    CURRENT_TEMPERATURE: string;
    FAN_STATUS: string;
    LAMP_STATUS: string;
    SCREEN_STATUS: string;
    SIGNAL_STATUS: string;
}

interface MonitoringSource {
    FAN: string;
    LAMP: string;
    SIGNAL: string;
    SCREEN: string;
    THERMOMETER: string;
}

interface KeyCode {
    ENERGY_SAVING: number;
    INPUT: number;
    LG_3D: number;
    NUM_0: number;
    NUM_1: number;
    NUM_2: number;
    NUM_3: number;
    NUM_4: number;
    NUM_5: number;
    NUM_6: number;
    NUM_7: number;
    NUM_8: number;
    NUM_9: number;
    OPT_1aA: number;
    CLEAR: number;
    VOL_UP: number;
    VOL_DOWN: number;
    MUTE: number;
    RATIO: number;
    AUTO: number;
    BRIGHTNESS_UP: number;
    BRIGHTNESS_DOWN: number;
    PSM: number;
    SMART_HOME: number;
    WBAL: number;
    MENU: number;
    SMENU: number;
    LEFT: number;
    UP: number;
    RIGHT: number;
    DOWN: number;
    ENTER: number;
    BACK: number;
    EXIT: number;
    SIMPLINK: number;
    TILE: number;
    STOP: number;
    REWIND: number;
    PLAY: number;
    PAUSE: number;
    FAST_FORWARD: number;
    RED: number;
    GREEN: number;
    YELLOW: number;
    BLUE: number;
    INFO: number;
}

declare class Signage {
    constructor();

    static readonly UNDEFINED: UNDEFINED;
    static readonly OsdPortraitMode: OsdPortraitMode;
    static readonly ImgResolution: ImgResolution;
    static readonly AutomaticStandbyMode: AutomaticStandbyMode;
    static readonly IsmMethod: IsmMethod;
    static readonly FailoverMode: FailoverMode;
    static readonly DigitalAudioInput: DigitalAudioInput;
    static readonly DpmMode: DpmMode;
    static readonly KeyOperationMode: KeyOperationMode
    static readonly EventType: EventType;
    static readonly MonitoringSource: MonitoringSource;
    static readonly KeyCode: KeyCode;

    setPortraitMode(success: () => void, failure: (error: Error) => void, options);
    setFailoverMode(success: () => void, failure: (error: Error) => void, options);
    getFailoverMode(success: () => void, failure: (error: Error) => void);
    setTileInfo(success: () => void, failure: (error: Error) => void, options);
    getTileInfo(success: () => void, failure: (error: Error) => void);
    getSignageInfo(success: () => void, failure: (error: Error) => void);
    enableCheckScreen(success: () => void, failure: (error: Error) => void, options);
    setIsmMethod(success: () => void, failure: (error: Error) => void, options);
    setDigitalAudioInputMode(success: () => void, failure: (error: Error) => void, options);
    registerSystemMonitor(success: () => void, failure: (error: Error) => void, options);
    unregisterSystemMonitor(success: () => void, failure: (error: Error) => void);
    getSystemMonitoringInfo(success: () => void, failure: (error: Error) => void);
    setPowerSaveMode(success: () => void, failure: (error: Error) => void, options);
    getPowerSaveMode(success: () => void, failure: (error: Error) => void);
    setUsagePermission(success: () => void, failure: (error: Error) => void, options);
    getUsagePermission(success: () => void, failure: (error: Error) => void);
    getUsageData(success: () => void, failure: (error: Error) => void);
    captureScreen(success: () => void, failure: (error: Error) => void, options);
    setIntelligentAuto(success: () => void, failure: (error: Error) => void, options);
    getIntelligentAuto(success: () => void, failure: (error: Error) => void);
    setStudioMode(success: () => void, failure: (error: Error) => void, options);
    getStudioMode(success: () => void, failure: (error: Error) => void);
    setLanDaisyChain(success: () => void, failure: (error: Error) => void, options);
    getLanDaisyChain(success: () => void, failure: (error: Error) => void);
    registerRS232CEventListener(success: () => void, failure: (error: Error) => void, options);
    unregisterRS232CEventListener(success: () => void, failure: (error: Error) => void);
    getNoSignalImageMode(success: () => void, failure: (error: Error) => void);
    setNoSignalImageMode(success: () => void, failure: (error: Error) => void, options);
    updateNoSignalImageList(success: () => void, failure: (error: Error) => void, options);
    resetNoSignalImage(success: () => void, failure: (error: Error) => void);
    addKeyItem(success: () => void, failure: (error: Error) => void, options);
    clearKeyTable(success: () => void, failure: (error: Error) => void);
    removeKeyItem(success: () => void, failure: (error: Error) => void, options);
    sendKey(success: () => void, failure: (error: Error) => void, options);
    getMirrorMode(success: () => void, failure: (error: Error) => void);
    setMirrorMode(success: () => void, failure: (error: Error) => void, options);
}
