interface Error {
    errorCode: string;
    errorText: string;
}

interface TimerType {
    ONTIMER: string;
    OFFTIMER: string;
}

interface TimerWeek {
    MONDAY: number;
    TUESDAY: number;
    WEDNESDAY: number;
    THURSDAY: number;
    FRIDAY: number;
    SATURDAY: number;
    SUNDAY: number;
    EVERYDAY: number;
}

declare class Time {
    constructor();

    static readonly TimerType: TimerType;
    static readonly TimerWeek: TimerWeek;

    reserveOnOffTimer(success: () => void, failure: (error: Error) => void, options);
    clearAllOnOffTimers(success: () => void, failure: (error: Error) => void);
    cancelOnOffTimer(success: () => void, failure: (error: Error) => void, options);
    getAllOnOffTimers(success: () => void, failure: (error: Error) => void);
    setHolidayScheduleMode(success: () => void, failure: (error: Error) => void, options);
    getHolidayScheduleMode(success: () => void, failure: (error: Error) => void);
    addHolidaySchedule(success: () => void, failure: (error: Error) => void, options);
    delHolidaySchedule(success: () => void, failure: (error: Error) => void, options);
    delAllHolidaySchedules(success: () => void, failure: (error: Error) => void);
    getAllHolidaySchedules(success: () => void, failure: (error: Error) => void);
    getHolidaySchedule(success: () => void, failure: (error: Error) => void);
    setHolidaySchedule(success: () => void, failure: (error: Error) => void, options);
    unsetHolidaySchedule(success: () => void, failure: (error: Error) => void);
}
