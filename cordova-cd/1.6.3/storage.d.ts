interface Error {
    errorCode: string;
    errorText: string;
}

type SCAP_URI = string;
type MAX_BUFFER_LENGTH = number;

interface AppMode {
    USB: string;
    LOCAL: string;
}

interface AppType {
    IPK: string;
    ZIP: string;
}

interface FileSystem {
    FAT32: string;
    NTFS: string;
}

interface GetStorageInfoResponse {
    free: number;
    total: number;
    used: number;
    externalMemory?: {
        [key: string]: {
            free?: number;
            used?: number;
            total?: number;
        }
    }
}

declare class Storage {
    constructor();

    static readonly SCAP_URI: SCAP_URI;
    static readonly MAX_BUFFER_LENGTH: MAX_BUFFER_LENGTH;
    static readonly AppMode: AppMode;
    static readonly AppType: AppType;
    static readonly FileSystem: FileSystem;

    downloadFirmware(success: () => void, failure: (error: Error) => void, options);
    upgradeFirmware(success: () => void, failure: (error: Error) => void);
    getFirmwareUpgradeStatus(success: () => void, failure: (error: Error) => void);
    changeLogoImage(success: () => void, failure: (error: Error) => void, options);
    upgradeApplication(success: () => void, failure: (error: Error) => void, options);
    removeApplication(success: () => void, failure: (error: Error) => void, options);
    copyFile(success: () => void, failure: (error: Error) => void, options);
    removeFile(success: () => void, failure: (error: Error) => void, options);
    listFiles(success: () => void, failure: (error: Error) => void, options);
    getStorageInfo(success: (response: GetStorageInfoResponse) => void, failure: (error: Error) => void);
    mkdir(success: () => void, failure: (error: Error) => void, options);
    exists(success: () => void, failure: (error: Error) => void, options);
    readFile(success: () => void, failure: (error: Error) => void, options);
    writeFile(success: () => void, failure: (error: Error) => void, options);
    statFile(success: () => void, failure: (error: Error) => void, options);
    removeAll(success: () => void, failure: (error: Error) => void, options);
    fsync(success: () => void, failure: (error: Error) => void, options);
    moveFile(success: () => void, failure: (error: Error) => void, options);
    unzipFile(success: () => void, failure: (error: Error) => void, options);
    getMD5Hash(success: () => void, failure: (error: Error) => void, options);
    decryptFile(success: () => void, failure: (error: Error) => void, options);
    formatUSB(success: () => void, failure: (error: Error) => void, options);
    getUSBInfo(success: () => void, failure: (error: Error) => void);
    importSettingData(success: () => void, failure: (error: Error) => void, options);
    exportSettingData(success: () => void, failure: (error: Error) => void, options);
}
