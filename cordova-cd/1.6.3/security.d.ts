interface Error {
    errorCode: string;
    errorText: string;
}

declare class Security {
    registerServerCertificate(success: () => void, failure: (error: Error) => void, options);
    unregisterServerCertificate(success: () => void, failure: (error: Error) => void, options);
    existServerCertificate(success: () => void, failure: (error: Error) => void, options);
    registerServerCertificateList(success: () => void, failure: (error: Error) => void, options);
    unregisterServerCertificateList(success: () => void, failure: (error: Error) => void, options);
    unregisterAllServerCertificateList(success: () => void, failure: (error: Error) => void);
    getServerCertificateList(success: () => void, failure: (error: Error) => void);
}
