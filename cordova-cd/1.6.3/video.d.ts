interface Error {
    errorCode: string;
    errorText: string;
}

declare class Video {
    constructor();

    getVideoStatus(success: () => void, failure: (error: Error) => void): void;
    setVideoSize(success: () => void, failure: (error: Error) => void, options);
    setContentRotation(success: () => void, failure: (error: Error) => void, options);
    getContentRotation(success: () => void, failure: (error: Error) => void);
    setVideoViewTransform(success: () => void, failure: (error: Error) => void, options);
    setRotatedVideoTransform(success: () => void, failure: (error: Error) => void, options);
}
