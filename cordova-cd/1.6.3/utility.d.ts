interface Error {
    errorCode: string;
    errorText: string;
}

interface CreateToastOptions {
    msg: string;
}

declare class Utility {
    createToast(success: () => void, failure: (error: Error) => void, options: CreateToastOptions): void;
}
