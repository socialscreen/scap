interface Error {
    errorCode: string;
    errorText: string;
}

interface PictureMode {
    VIVID: string;
    STANDARD: string;
    APS: string;
    CINEMA: string;
    GAME: string;
    SPORTS: string;
    EXPERT1: string;
    EXPERT2: string;
}

interface AppMode {
    LOCAL: string;
    USB: string;
    REMOTE: string;
}

interface AppType {
    IPK: string;
    ZIP: string;
}

interface SetPicturePropertyOptions {
    backlight?: number;
    contrast?: number;
    brightness?: number;
    sharpness?: number;
    hSharpness?: number;
    vSharpness?: number;
    color?: number;
    tint?: number;
    colorTemperature?: number;
    dynamicContrast?: string;
    superResolution?: string;
    colorGamut?: string;
    dynamicColor?: string;
    noiseReduction?: string;
    mpegNoiseReduction?: string;
    blackLevel?: string;
    gamma?: string;
}

declare class Configuration {
    constructor();

    static readonly PictureMode: PictureMode;
    static readonly AppMode: AppMode;
    static readonly AppType: AppType;

    setPictureMode(success: () => void, failure: (error: Error) => void, options: { mode: string });
    getPictureMode(success: () => void, failure: (error: Error) => void);
    setPictureProperty(success: () => void, failure: (error: Error) => void, options: SetPicturePropertyOptions);
    getPictureProperty(success: () => void, failure: (error: Error) => void);
    setProperty(success: () => void, failure: (error: Error) => void, options);
    getProperty(success: () => void, failure: (error: Error) => void, options);
    setCurrentTime(success: () => void, failure: (error: Error) => void, options);
    getCurrentTime(success: () => void, failure: (error: Error) => void);
    restartApplication(success: () => void, failure: (error: Error) => void);
    getServerProperty(success: () => void, failure: (error: Error) => void);
    setServerProperty(success: () => void, failure: (error: Error) => void, options: { appLaunchMode?: string, appType?: string });
    clearCache(success: () => void, failure: (error: Error) => void);
    getTimeZoneList(success: () => void, failure: (error: Error) => void);
    getTimeZone(success: () => void, failure: (error: Error) => void);
    setTimeZone(success: () => void, failure: (error: Error) => void, options);
    debug(success: () => void, failure: (error: Error) => void, options);
    setUSBLock(success: () => void, failure: (error: Error) => void, options: { enabled: boolean });
    getUSBLock(success: () => void, failure: (error: Error) => void);
    setOSDLock(success: () => void, failure: (error: Error) => void, options);
    getOSDLock(success: () => void, failure: (error: Error) => void);
    getLocaleList(success: () => void, failure: (error: Error) => void);
    setOSDLanguage(success: () => void, failure: (error: Error) => void, options);
    getOSDLanguage(success: () => void, failure: (error: Error) => void);
    setVirtualKeyboardLanguage(success: () => void, failure: (error: Error) => void, options: { languageCodeList: string[] });
    getVirtualKeyboardLanguage(success: () => void, failure: (error: Error) => void);
    setProxyBypassList(success: () => void, failure: (error: Error) => void, options);
    getProxyBypassList(success: () => void, failure: (error: Error) => void);
}
