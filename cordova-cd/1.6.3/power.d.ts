interface Error {
    errorCode: string;
    errorText: string;
}

interface PowerCommand {
    SHUTDOWN: string;
    REBOOT: string;
}

interface DisplayMode {
    DISPLAY_OFF: string;
    DISPLAY_ON: string;
}

interface TimerWeek {
    MONDAY: number;
    TUESDAY: number;
    WEDNESDAY: number;
    THURSDAY: number;
    FRIDAY: number;
    SATURDAY: number;
    SUNDAY: number;
    EVERYDAY: number;
}

interface DPMSignalType {
    CLOCK: string;
    CLOCK_WITH_DATA: string;
}

interface PMMode {
    PowerOff: string;
    SustainAspectRatio: string;
    ScreenOff: string;
    ScreenOffAlways: string;
    ScreenOffBacklight: string;
}

interface ExecutePowerCommandOptions {
    powerCommand: string;
}

declare class Power {
    constructor();

    static readonly PowerCommand: PowerCommand;
    static readonly DisplayMode: DisplayMode;
    static readonly TimerWeek: TimerWeek;
    static readonly DPMSignalType: DPMSignalType;
    static readonly PMMode: PMMode;

    getPowerStatus(success: () => void, failure: (error: Error) => void);
    enableAllOnTimer(success: () => void, failure: (error: Error) => void, options);
    enableAllOffTimer(success: () => void, failure: (error: Error) => void, options);
    enableWakeOnLan(success: () => void, failure: (error: Error) => void, options);
    addOnTimer(success: () => void, failure: (error: Error) => void, options);
    deleteOnTimer(success: () => void, failure: (error: Error) => void, options);
    addOffTimer(success: () => void, failure: (error: Error) => void, options);
    deleteOffTimer(success: () => void, failure: (error: Error) => void, options);
    getOnTimerList(success: () => void, failure: (error: Error) => void);
    getOffTimerList(success: () => void, failure: (error: Error) => void);
    setDisplayMode(success: () => void, failure: (error: Error) => void, options);
    executePowerCommand(success: () => void, failure: (error: Error) => void, options: ExecutePowerCommandOptions);
    setDPMWakeup(success: () => void, failure: (error: Error) => void, options);
    getDPMWakeup(success: () => void, failure: (error: Error) => void);
    setPMMode(success: () => void, failure: (error: Error) => void, options);
    getPMMode(success: () => void, failure: (error: Error) => void);
    setPowerOnDelay(success: () => void, failure: (error: Error) => void, options);
    getPowerOnDelay(success: () => void, failure: (error: Error) => void);
    getOnOffTimeSchedule(success: () => void, failure: (error: Error) => void);
    setOnOffTimeSchedule(success: () => void, failure: (error: Error) => void, options);
    unsetOnOffTimeSchedule(success: () => void, failure: (error: Error) => void);
}
